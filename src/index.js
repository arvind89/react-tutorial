import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter,Route, Link, withRouter} from "react-router-dom";

import Navbar from "./component/Navbar";
import Welcome from "./component/Welcome";
import Footer from "./component/Footer";
import CreateArticle from "./component/CreateArticle";
import Login from "./component/Login";
import SignUp from "./component/SignUp";
import AuthService from '././services/auth'
import ArticlesService from './services/articles';

import Auth from "./component/Auth"
import RedirectIfAuth from "./component/RedirectIfAuth";
import UserArticles from "./component/UserArticles";
import NotificationService from './services/notification'


class App extends React.Component{

    constructor(){
        super();

        this.state = {
            authUser:null,
            articles:[]
        }
    }

    setAuthUser = (authUser)=>{
        console.log('set auth user',)
        
        this.setState({
             authUser
        },()=>{
            this.props.notyService.success("LoggedIn successfully !!!!");
            localStorage.setItem('user',JSON.stringify(authUser));
            this.props.history.push('/');
        })
           
        


    }

    setArticles = (article)=>{
     this.setState({
         articles:article
     })
    }


    removeAuthUser = ()=>{
        
        localStorage.removeItem('user');
        this.props.notyService.success("Succefully loggedOut !!!")
       
        this.setState({
           authUser:null
       })
    }

    componentDidMount(){
        const user = localStorage.getItem('user');
        console.log("user ",user)

        if(user){
             
            this.setState({
                authUser:JSON.parse(user)
            })
        }
    }

    render(){
        const { location }  = this.props;
        return(
 
            <div>
            
                        {
                            location.pathname !== '/login' && location.pathname!=='/signup' &&
                            <Navbar 
                            authUser = {this.state.authUser}
                            removeAuthUser = {this.removeAuthUser}
                            />
            
                        }
                    
                        
                       <Route exact path="/" 
                       
                       render  = {
                           (props)=>
                           <Welcome 
                           {...props}
                           getArticles = {this.props.articleService.getArticles}
                           />
                       }
                       
                        />
                       <Route path="/about" component={About} />
                       <Route path="/home" component={Home} />
                      
                       <RedirectIfAuth 
                       
                       path="/login" 
                       component = {Login}

                        props = {{
                        setAuthUser : this.setAuthUser,
                        loginUser : this.props.authService.loginUser,
                       }}
                       isAuthenticated = {this.state.authUser!==null}
                       />

                       <RedirectIfAuth 
                       
                       path="/signup" 
                       component = {SignUp}
                       props = {{
                        setAuthUser : this.setAuthUser,
                        registerUser : this.props.authService.registerUser
                       }}
                       isAuthenticated = {this.state.authUser!==null}
                       />
                      
                      
                      

                      <Auth   
                      path = "/articles/create"
                      component = {CreateArticle}

                      props = {{
                        getArticleCategories:this.props.articleService.getArticleCategories,
                        createArticle : this.props.articleService.createArticle,
                        token:'sjdfksdjfksdjfksdjfsdjfkasjdfksjafsfdsfsdf'
                      }}
                      isAuthenticated = {this.state.authUser!==null}
                      
                      />

                      <Auth   
                      path = "/articles/edit/:slug"
                      component = {CreateArticle}

                      props = {{
                        getArticleCategories:this.props.articleService.getArticleCategories,
                        createArticle : this.props.articleService.createArticle,
                        token:'sjdfksdjfksdjfksdjfsdjfkasjdfksjafsfdsfsdf',
                        articles:this.state.articles
                        
                        
                      }}
                      isAuthenticated = {this.state.authUser!==null}
                      
                      />


                       <Auth   
                      path = "/user/articles"
                      component = {UserArticles}

                      props = {{
                        getUserArticles:this.props.articleService.getUserArticles,
                        setArticles:this.setArticles,
                        deleteArticle:this.props.articleService.deleteArticle,
                        token: 'sjdfksdjfksdjfksdjfsdjfkasjdfksjafsfdsfsdf'
                        
                      }}
                      isAuthenticated = {this.state.authUser!==null}
                      
                      />
                      
                      
                    
                       {
                             location.pathname !== '/login' && location.pathname!=='/signup' &&
                            <Footer />
            
                        }
                      
                       </div>
        )
    }
}

// create highorder component

const Main = withRouter((props)=>{
    return(
       <App 
       authService = {new AuthService()}  
       articleService = {new ArticlesService()}
       notyService = {new NotificationService()}
       
       {...props}/> 
    );
});


const About = ()=>{
    return <h1>This is the About page</h1>
}

const Home = ()=>{
    return <h1>This is the Home page</h1>
}

ReactDOM.render(
    
   <BrowserRouter>
   <Main />
   </BrowserRouter>
    
    
    , document.getElementById('root'));
registerServiceWorker();
