import React from "react";
import {Link} from "react-router-dom"


const Article = ({item})=>{

    return (
        <article className="mt-90">
        <header className="text-center mb-40">
          <h3>
            <Link to={`article/${item.slug}`}>{item.title}></Link>
          </h3>
          <div className="link-color-default fs-12">
            
        <time>{(new Date(item.created_at)).toDateString()}</time>
          </div>
        </header>
        <a href="blog-single.html">
          <img className="rounded" src={item.imageUrl} alt="..." />
        </a>
        <div className="card-block">
          <p className="text-justify">{item.content}</p>
          <p className="text-center mt-40">
            <Link className="btn btn-primary btn-round" to={`article/${item.slug}`}>Read more</Link>
          </p>
        </div>
      </article>
    );
}

export default Article;