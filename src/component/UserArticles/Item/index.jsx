import React from 'react'
import Banner from '../../Banner'
import Article from '../../Article'

const Item = ({articles, handlePagination,nextUrl, preUrl, handleDeleteArticle,handleEditArticle})=>{



    return(
        
        <div>
           
      <Banner
      
      backgroundImage = {`url(${process.env.PUBLIC_URL}/assets/img/bg-gift.jpg)`}    
      title = "Arvind Kumar"
      subTitle = "Here are the articles" />



      <main className="main-content bg-gray">
      <div className="row">
        <div className="col-12 col-lg-6 offset-lg-3">

       
         
          {articles && articles.map(article => (
          <div key={article.id}>
            
         <Article item={article} />
        
         <div className="text-center">
          <button className="btn btn-info mr-5" onClick = {()=>handleEditArticle(article)}>Edit Articles</button>
            <button className="btn btn-danger" onClick={()=>handleDeleteArticle(article.id)}>Delete Articles</button>
           </div>
           
         <hr/>
         
          </div>))}
        
          <nav className="flexbox mt-50 mb-50">

          <a className={`btn btn-white ${preUrl?'':'disabled'}`} href="#" onClick = {()=>handlePagination(preUrl)}>
            <i className="ti-arrow-left fs-9 ml-4" />Previous Page
            </a>

            <a className={`btn btn-white${nextUrl==null?'disabled':''} `} href="#" onClick = {()=>handlePagination(nextUrl)}>
            Next Page<i className="ti-arrow-right fs-9 mr-4" /> </a>
           
          </nav>
        </div>
      </div>
      </main>

      </div>


    )
};


export default Item;