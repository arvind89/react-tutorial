import React from 'react';
import Item from './Item'

class UserArticles extends React.Component{
    
    
      constructor(){
        super();
        this.state = {
          articles:{},
          token:null
        }
      }
    
     async componentWillMount(){
    
        const articles  = await this.props.getUserArticles(this.props.token);
        this.state.token = this.props.token;
    
        this.setState({
          articles
        })

        this.props.getUserArticles(articles.data)
    
      }

      handleDeleteArticle =  async (id)=>{
    

       this.props.deleteArticle(id,this.props.token);
      }



      handleEditArticle  = async (article)=>{
      this.props.setArticles(article)
       this.props.history.push(`/articles/edit/${article.slug}`)
      }
    
    
      handlePagination = async (url)=>{
         
         const articles  = await this.props.getUserArticles(this.props.token, url);
         this.setState({articles })
      }
    
      render(){
      
        return(
          <Item
           articles = {this.state.articles.data}
           nextUrl = {this.state.articles.next_page_url}
           preUrl = {this.state.articles.prev_page_url}
           handlePagination = {this.handlePagination}
           handleDeleteArticle = {this.handleDeleteArticle}
           handleEditArticle = {this.handleEditArticle}
          />
        )
      }
    }
    
    
    
    
    export default UserArticles;