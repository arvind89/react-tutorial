import React from "react";
import Article from "./../Article";
import Banner from "./../Banner"
import Articles from './Articles'

class Welcome extends React.Component{


  constructor(){
    super();
    this.state = {
      articles:{}
    }
  }

 async componentDidMount(){

    const articles  = await this.props.getArticles();
    console.log('articles response ',articles);

    console.log('next url ',articles.next_page_url)

    this.setState({
      articles
    })

  }


  handlePagination = async (url)=>{
     console.log('url....',url);
     const articles  = await this.props.getArticles(url);

     console.log('url data ....',JSON.stringify(articles))

     this.setState({articles })
  }

  render(){
    console.log('render dom',this.state.articles)
    return(
      <Articles
       articles = {this.state.articles.data}
       nextUrl = {this.state.articles.next_page_url}
       preUrl = {this.state.articles.prev_page_url}
       handlePagination = {this.handlePagination}
      />
    )
  }
}




export default Welcome;