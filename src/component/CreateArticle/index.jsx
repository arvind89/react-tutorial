import React from "react";
import Banner from "./../Banner";
import CreateArticleForm from './CreateArticleForm'

class CreateArticle extends React.Component{

  constructor(){
    super();
    this.state = {
      image:null,
      content:'',
      title:'',
      category:null,
      errors:{},
      categories:[],
      editing:false,
      articles:[],
      

    }
  }


  async componentDidMount(){

    const articleCategories = await this.props.getArticleCategories();
   
    if(this.props.match.params.slug){
       this.setState({
           editing:true,
           articles:this.props.articles,
           categories:articleCategories
        })
    }else{

        this.setState({
            categories:articleCategories
          })
    }
    
    
  }

  updateArticle = async ()=>{
      console.log('update article ')
  }

  handleSubmit = async (event)=>{

    event.preventDefault();
    console.log("create article ",this.state)

    try{
      const response =  await this.props.createArticle(this.state);
    }catch(errors){
            console.log("errors is ",errors);
            this.setState({
              errors
            })


    }

   
   
  }

  handleInputChange = (event)=>{
    console.log("input file ",event.target.files)
    
   this.setState({
       [event.target.name]:event.target.type==='file'?event.target.files[0]:event.target.value
   })
}

  render (){
    return(
      <CreateArticleForm 
      handleInputChange = {this.handleInputChange}
      categories = {this.state.categories}
      handleSubmit = {this.handleSubmit}
      editing = {this.state.editing}
      article = {this.state.articles}
      updateArticle = {this.updateArticle}
      errors = {this.state.errors}
    
      
      />
    )
    
  }
}

export default CreateArticle;