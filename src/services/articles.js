import Axios from 'axios';
import  config from '../config'
import {validateAll} from "indicative";

export default class ArticlesService{


   async getArticleCategories(){
 
    const categories = {
        data:[
            {'id':1,'name':'arvind'},
            {'id':2,'name':'vijay'}
        ]
    }
    

    //const categories = await Axios.get(`${config.apiUrl}/articles`);
  
    

    return categories.data
        
    }


    async getArticles(url = `${config.apiUrl}/articles`){

        console.log('url#########33',url)

     const response  = await Axios.get(url);
     console.log('articles #####',response)

     return response.data.data;


    }

    async getUserArticles(token, url = `${config.apiUrl}/articles`){

       const response = await Axios(url, {
           headers:{
               Authorization:`Bearer ${token}`
           }
       });
       return response.data.data
       }


       async deleteArticle(){
           console.log('delete articles ')
       }

    createArticle = async (data,token)=>{
        console.log("data image",data.image)

        if(!data.image){
            const formattedErrors = {}
            formattedErrors['image'] = 'image is required';
            console.log("errors", formattedErrors)
            return Promise.reject(formattedErrors)
            
        }

        try{

            const rules = {
                title: 'required',
                content: 'required',
                category:'required'
            }
         
            const messages = {
         
             required: 'the {{field}} is required'
         
            }
         
            await validateAll(data, rules, messages)
            const image =  await this.uploadCloudniary(data.image)

            const response = await Axios.post(`${config.apiUrl}/articles`,{
                title:data.title,
                content:data.content,
                image:image.secure_url,
                category_id:data.category 
            },
            {
            
                headers:{
                    Authorization:`Bearer ${token}`
                }
            }
         )

         return response.data;
             

        }catch(errors){
            console.log(errors)
            if(errors.response){
                return Promise.reject(errors.response.data)
            }else{
                const formattedErrors = {}
                errors.forEach(errors=> formattedErrors[errors.field] = errors.message);
                console.log("errors", formattedErrors)
                return Promise.reject(formattedErrors)
            }

          
        }
    }


    updateArticle = async (data,article,token)=>{
        console.log("data image",data.image)
        let image;

        if(data.image){
           
            image = await this.uploadCloudniary(data.image)
        }

        try{

            const rules = {
                title: 'required',
                content: 'required',
                category:'required'
            }
         
            const messages = {
         
             required: 'the {{field}} is required'
         
            }
         
            await validateAll(data, rules, messages)
            
            const response = await Axios.post(`${config.apiUrl}/articles/${article.id}`,{
                title:data.title,
                content:data.content,
                imageUrl: image ? image.secure_url : article.imageUrl,
                category_id:data.category 
            },
            {
            
                headers:{
                    Authorization:`Bearer ${token}`
                }
            }
         )

         return response.data;
             

        }catch(errors){
            console.log(errors)
            if(errors.response){
                return Promise.reject(errors.response.data)
            }else{
                const formattedErrors = {}
                errors.forEach(errors=> formattedErrors[errors.field] = errors.message);
                console.log("errors", formattedErrors)
                return Promise.reject(formattedErrors)
            }

          
            


        }
    }

  

        async uploadCloudniary(image){
            const form = new FormData();
    
            form.append('file',image);
            form.append('upload_preset','d2nebn17');
            
            const response = await Axios.post('https://api.cloudinary.com/v1_1/durlifabf/image/upload',form);
    
            console.log('response ',response);
    
            return response.data;
        }




}