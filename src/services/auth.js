
import {validateAll} from "indicative";
import Axios from 'axios';
import config from '../config'

export default class AuthService {

   
async registerUser(data){

 
 const rules = {
   
   name:"required|string",
   email:"required|email",
   password:"required|string|min:6|confirmed",
   
 };

 const message = {
   required:'This {{field}} is required',
   'email.email':'The email is not valid',
   'password.min':'Password lenght should be 6 charector loong',
   'password.confirmed':'The password confirm does not match',
   
 }

 try{

      await  validateAll(data,rules,message);
      const response =  await Axios.post(`${config.apiUrl}/signup`,{
      name:data.name,
      email:data.email,
      password: data.password
  
    });

     console.log('repsonse data ',response.data)
     return response.data;

    
     
   }catch(errors){

    const formattedErrors = {};
   
    if(errors.status === 401){
    formattedErrors['email'] = errors.response.data['email'][0];
    return Promise.reject(formattedErrors);
    }else{

        const formattedErrors = {}
        errors.forEach(errors=> formattedErrors[errors.field] = errors.message);
        console.log("errors", formattedErrors)
        return Promise.reject(formattedErrors)
    }
    
    // this.setState({
    //   errors:formattedErrors
    // })
   }

 

}

async loginUser(data){

  console.log("login user ",data)
  
   // set of data for validation
   //const data = this.state;
   // set of rules for validaiton 
   const rules = {
     email:"required|email",
     password:"required|string|min:6",
     
   };
  
   const message = {
     required:'This {{field}} is required',
     'email.email':'The email is not valid',
     'password.min':'Password lenght should be 6 charector loong',
    
     
   }
  
   try{
  
        await  validateAll(data,rules,message);
        const response =  await Axios.post(`${config.apiUrl}/login`,{
        email:data.email,
        password: data.password
    
      });
  
      console.log('repsonse data ',response.data)
       return response.data;
  
      
       
     }catch(errors){
  
      const formattedErrors = {};
     
      if(errors.status === 401){
      formattedErrors['email'] = 'Invalid credentials'
      return Promise.reject(formattedErrors);
      }else{
  
          const formattedErrors = {}
          errors.forEach(errors=> formattedErrors[errors.field] = errors.message);
          console.log("errors", formattedErrors)
          return Promise.reject(formattedErrors)
      }
      
      // this.setState({
      //   errors:formattedErrors
      // })
     }
  
   
  
  }
}